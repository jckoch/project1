% Project 1
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%   
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% ---- TASK 1 ------------------------------------------------------------
% Truss finite element analysis
clear all; clc;

%% Generate the model
ndof = 24; % number of degrees of freedom
nele = 19; % number of elements

% define the topology matrix Edof
Edof = [1 1 2 3 4;
        2 3 4 5 6;
        3 5 6 7 8;
        4 7 8 9 10;
        5 9 10 11 12;
        6 11 12 13 14;
        7 3 4 15 16;
        8 3 4 17 18;
        9 5 6 17 18;
        10 7 8 17 18;
        11 7 8 19 20;
        12 7 8 21 22;
        13 9 10 21 22;
        14 11 12 21 22;
        15 11 12 23 24;
        16 15 16 17 18;
        17 17 18 19 20;
        18 19 20 21 22;
        19 21 22 23 24];

% define the element coordinates
ex = [0 3;
      3 9.5;
      9.5 16;
      16 22.5;
      22.5 29;
      29 32;
      3 3;
      3 9.5;
      9.5 9.5;
      16 9.5;
      16 16;
      16 22.5;
      22.5 22.5;
      29 22.5;
      29 29;
      3 9.5;
      9.5 16;
      16 22.5;
      22.5 29];
ey = [4 4;
      4 4;
      4 4;
      4 4;
      4 4;
      4 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 0;
      0 0;
      0 0;
      0 0];

% initialize the stiffness matrix K and load vector f
K = zeros(ndof);
fl = zeros(ndof,1);

% define the element properties
R = [0.025 0.010];  % m
A= pi*R.^2; % m2
E=200e9;  % GPa
ep=[E A];

% plot undeformed truss design
figure(1);
plotpar = [1, 2, 1];
elnum = Edof(:,1);
eldraw2(ex,ey,plotpar,elnum);
hold on;
 
%% ---- Stiffness Matrix Generation ---------------------------------------
% first step is to calculate the element stiffness matrices, Ke, and 
% assemble into K

red_A_elements = [9 11 13];
if isempty(red_A_elements)
    fprintf('Radius is %.3f mm^2\n', R(1)*10^3);
else
    fprintf('Elements with reduced x-section are ');
    for j = 1:length(red_A_elements)
        if j == length(red_A_elements)
            fprintf('%d\n', red_A_elements(j));
        else
            fprintf('%d, ', red_A_elements(j));
        end
    end
    fprintf('Radii are %.3f mm and %.3f mm\n', R(1)*10^3, R(2)*10^3);
    fprintf('\t,where the smaller radius correspond to\n\t the reduced x-section members\n\n');
end

for i=1:length(Edof)
    if ~ismember(i, red_A_elements)
        epi = ep(1:2);
    else
        epi = [ep(1) ep(3)];
    end
    Ke = bar2e(ex(i,:),ey(i,:),epi);
    K = assem(Edof(i,:),K,Ke);
end

% solve the system of equations with the given boundary conditions
bc = [1 0;2 0;13 0;14 0;15 0;16 0;23 0;24 0]; % BCs

%% ---- Task 1: A to D ----------------------------------------------------
% define applied load on truss
m = 1500; % kg
g = 9.81; % m^2/s
fl(8) = -m*g;

%------calclulate the total mass-------------------------------------------
L=zeros(1, length(A));
for i=1:length(Edof)
    if ~ismember(i, red_A_elements)
        L(1)=L(1)+sqrt((ex(i,2)-ex(i,1))^2+(ey(i,2)-ey(i,1))^2);
    else
        L(2)=L(2)+sqrt((ex(i,2)-ex(i,1))^2+(ey(i,2)-ey(i,1))^2);
    end
end
density = 7850; % steel S275, kg/m3
mass = (A(1)*L(1) + A(2)*L(2))*density;
fprintf("Total mass: %.2f kg\n", mass)

% solve the system of equations using CALFEM solveq function
[a, ~] = solveq(K, fl, bc);

% determine the element forces from the nodal reaction forces, Q
ed = extract(Edof,a);
N = zeros(length(Edof),1);
for i=1:length(Edof)
    N(i,:) = bar2s(ex(i,:),ey(i,:),ep,ed(i,:));
    % fprintf("%2.1f | %1.6f kN\n", i, N(i,:)/1000)
end

fprintf("Deflection at dof %2.1f is %1.6f mm\n", 8, a(8,:)*1000);
fprintf("\n");

%----- Draw deformed shape ------------------------------------------------
plotpar=[2,4,1];
eldisp2(ex,ey,ed,plotpar);
hold off;

%% ---- Task 1: E ---------------------------------------------------------
% define presribed displacement acting at midspan of the truss
pc = [8 -0.002]; % acting at the 8th dof

% solve the system of equations using MY OWN FUNCTION
[a, Q] = mysolveq(K, fl, bc, pc);

%----- Element forces -----------------------------------------------------
% determine the displacements per element
ed = extract(Edof,a);

%----- Draw deformed shape ------------------------------------------------
figure(2)
plotpar=[1,2,1];
eldraw2(ex,ey,plotpar,elnum);
hold on;

plotpar=[2,4,1];
eldisp2(ex,ey,ed,plotpar);
hold off;

%% ---- TASK 2 ------------------------------------------------------------
% 1D Heat conduction
clear all; clc;

%% define general parameters
% thickness of materials within wall
h1 = 0.4;       % m
h2 = 0.2;       % m
h3 = 0.009;     % m
h = h1 + h2 + h3;

% material thermal conductivities
k_c = 1.7;      % concrete, W/(m K)
k_mw = 0.04;    % mineral wool, W/(m K) from https://engineeringtoolbox.com
k_p = 0.22;      % plaster, W/(m K) from https://engineeringtoolbox.com

% define variable thermal conductivities
k1 = @(x) (0.6 + 0.4*(x/h1))*k_c;   % valid from 0 to h1
k2 = k_mw;                          % valid from h1 to h2
k3 = k_p;                           % valid from h2 to h3

% define cross-sectional area, A
A = 1;          % unit area, m^2

%% discretise the domain of the problem
dx = 0.001;         % define the number of elements <user input>
nel_1 = h1/dx;      % number of elements within concrete material
nel_2 = h2/dx;      % number of elements within mineral wool material
nel_3 = h3/dx;      % number of elements within plaster material
nel = nel_1 + nel_2 + nel_3; % total number of elements
ndof = nel + 1;     % 1D FEA so 1 dof for temperature at each node

%% define the global element coordinates and the topology matrix, Edof
ex = [linspace(0, h1, nel_1) linspace(h1+dx, h1+h2, nel_2) linspace(h1+h2+dx, h, nel_3)]';

Edof = zeros(nel,3);
Edof(:, 1) = (1:nel)';
Edof(:, 2) = (1:nel)';
Edof(:, 3) = (2:nel+1)';

%% define loading data
Q = 0;              % NO external heat supply

%% initialize matrices
K = zeros(ndof);        % global stiffness matrix, K
fl = zeros(ndof, 1);    % load vector, fl

%% calculate the element stiffness matrix and assemble into K
% also calculate the element load vector and assemble into fl
x1 = 0; x2 = dx;
[~, n] = size(Edof); B = [-1 1];
k = k_c * 0.6;
for i = 1:length(Edof)
    if i <= nel_1
        % ---- NOTE ------------------------------------------------------- 
        % integrating from x_i+1 to x_i is NOT equivalent to 
        % integrating from 0 to dx
        % -----------------------------------------------------------------
        k = k + integral(k1,x1,x2);
        x1 = x1 + dx; x2 = x2 + dx;
    elseif (i > nel_1) && (i <= nel_1+nel_2)
        k = k2;
    else
        k = k3;
    end
    % fprintf('%d: k = %.6f\n', i, k); % DEBUG
    Ke = (k/dx)*B'*B;
    K(Edof(i,2:n), Edof(i,2:n)) = K(Edof(i,2:n), Edof(i,2:n)) + Ke;
end

%% define the boundary conditions

% Essential (Dirichlet) boundary conditions
bc = [1 -5];            % outside wall surface temperature 

% Convective (Robin) boundary conditions
Text = 15;              % inside air temperature
alpha = 8;              % standard thermal surface resistance, W/(m^2 K)

%% calculate boundary load vector
Nb = zeros(ndof, 1);
Nb(ndof, 1) = 1;        % the shape function over the last element will
                        % be 1 at x=L and 0 elsewhere.
fb = alpha*Text*A*Nb;

%% calculate convective stiffness matrix
Kc = alpha*A*(Nb*Nb');

%% establish system of equations and solve
K = K + Kc;
f = fb + fl;

% solve system of equations
[T, Q] = solveq(K, f, bc);

%% calculate temperature distribution along the bar
% extract temperature on each element
ed = extract(Edof,T);
eq = extract(Edof, Q);

% plot the temperature as a function of thickness of the wall
figure(3);
plot(ex(:, 1), ed(:, 1));
xlabel('x [m]');
ylabel('Temperature [deg C]');
title('Temperature through the thickness of a wall');

%% ---- TASK 3 ------------------------------------------------------------
% 1D Elasticity
clear all; clc;

%% define general parameters
% thickness of materials within wall
L = 1.0;       % m

% elasticity of modulus
E = 80*10^9;   % Pa

% density
rho = 2800;     % kg/m^3

% define cross-sectional area, A
Ao = 0.01;     % unit area, m^2

%% Run my FE-program for 1D heat flow and elasticity
dx = [0.5 0.25 0.1 0.01];
mkr = ['o','x','*','+'];
F = 0;
max_nel = L/dx(end);
u = zeros(max_nel,length(dx));
Q = zeros(max_nel,length(dx));
for i = 1:length(dx)
    [d, r, ex, ndof] = heat1Deqn(dx(i), F, L, E, rho, Ao);

    u(1:ndof, i) = d;
    Q(1:ndof, i) = r;
    X(1:ndof, i) = ex';
      
    % plot displacement distribution along the bar
    figure(4);
    plot(X(1:ndof, i), u(1:ndof, i), mkr(i));
    hold on;
end

figure(4);
plot(x,un);
xlabel('x [m]');
ylabel('u [m]');
legend('nel = 2', 'nel = 4', 'nel = 10', 'nel = 100', 'Analytical Solution', 'Location', 'northwest');
title('Displacement over the length of the bar (Varying area A)');
hold off;

figure(5);
umid = [u(2,1) u(3,2) u(6,3) u(50,4)];
nel = [2 4 10 100];
plot(nel, umid);
xlabel('Number of Elements');
ylabel('u @ midspan [m]');
title('Displacement at x = L/2 for varying number of elements');