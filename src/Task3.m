% Project 1: Task 3
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%   
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% ---- TASK 3 ------------------------------------------------------------
% 1D Elasticity
clear all; clc;

%% define general parameters
% thickness of materials within wall
L = 1.0;       % m

% elasticity of modulus
E = 80*10^9;   % Pa

% density
rho = 2800;     % kg/m^3

% define cross-sectional area, A
Ao = 0.01;     % unit area, m^2

%% Run my FE-program for 1D heat flow and elasticity
dx = [0.5 0.25 0.1 0.01];
mkr = ['o','x','*','+'];
F = 0;
max_nel = L/dx(end);
u = zeros(max_nel,length(dx));
Q = zeros(max_nel,length(dx));
for i = 1:length(dx)
    [d, r, ex, ndof] = heat1Deqn(dx(i), F, L, E, rho, Ao);

    u(1:ndof, i) = d;
    Q(1:ndof, i) = r;
    X(1:ndof, i) = ex';
      
    % plot displacement distribution along the bar
    figure(4);
    plot(X(1:ndof, i), u(1:ndof, i), mkr(i));
    hold on;
end

figure(4);
plot(x,un);
xlabel('x [m]');
ylabel('u [m]');
legend('nel = 2', 'nel = 4', 'nel = 10', 'nel = 100', 'Analytical Solution', 'Location', 'northwest');
title('Displacement over the length of the bar (Varying area A)');
hold off;

figure(5);
umid = [u(2,1) u(3,2) u(6,3) u(50,4)];
nel = [2 4 10 100];
plot(nel, umid);
xlabel('Number of Elements');
ylabel('u @ midspan [m]');
title('Displacement at x = L/2 for varying number of elements');