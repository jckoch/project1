%Finite Element Method - Basics, VSM167
%Project 1: One-dimensional FE problems
%Task d: One-dimensional elasticity (analytical vs FEM solution for varying area)
%Created by: Dimosthenis Floros
%Date: 2014-10-28
%Modified 2018-10-23 by Martin Fagerstr�m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Solve the analytical problem

syms x L rho g Emod Ao C1 C2

f=(x-3*x^2/(8*L))/(1-3*x/(4*L));

intf=int(f,'x');

u(x)=-rho*g/Emod*intf-(4/3)*L*log(1-3*x/(4*L))*C1/Ao+C2;

dudx(x)=diff(u,'x');

S=solve(u(0)==0,dudx(L)==0,C1,C2);

unew=subs(u,[C1,C2],[S.C1,S.C2]);

L=1;        %modify for your length
x=0:0.01:L; 

%add your values
rho=2800;   %density
g=9.81;     %gravity
Emod=80*10^9;  %Young's modulus
Ao=0.01;    %Initial area
un=eval(unew);
figure(3);
plot(x,un,'--')

xlabel('x [m]')
ylabel('u [m]')
title('Displacement over the length of the bar (Varying area A)')

