function [u, Q, ex, ndof]=heat1Deqn(dx, F, L, E, rho, Ao)
    %1Dheateqn

    %% define general parameters
    % gravitational constant
    g = 9.81; % m/s^2
    
    A1 = @(x) (1 - 0.75*(x/L));
    
    %% discretise the domain of the problem
    nel = L/dx;         % define the number of elements <user input>
    ndof = nel + 1;     % 1D FEA so 1 dof for temperature at each node

    %% define the global element coordinates and the topology matrix, Edof
    ex = linspace(0, L, nel+1);

    Edof = zeros(nel,3);
    Edof(:, 1) = (1:nel)';
    Edof(:, 2) = (1:nel)';
    Edof(:, 3) = (2:nel+1)';

    %% initialize matrices
    K = zeros(ndof);        % global stiffness matrix
    fl = zeros(ndof, 1);    % load vector
    fb = zeros(ndof, 1);    % boundary load vector

    %% calculate the element stiffness matrix and assemble into K
    % also calculate the element load vector and assemble into fl
    x1 = 0; x2 = dx;
    [~, n] = size(Edof); B = [-1 1];
    for i = 1:nel
        % Calculate element stiffness matrix and assemble into K
        % ---- NOTE -----------------------------------------------------------
        % integrating from x_i+1 to x_i is NOT equivalent to 
        % integrating from 0 to dx
        % ---------------------------------------------------------------------
        Ae = integral(A1,x1,x2);

        Ke = ((Ao*E)/dx^2)*Ae*B'*B;
        K(Edof(i,2:n), Edof(i,2:n)) = K(Edof(i,2:n), Edof(i,2:n)) + Ke;

        % Calculate element load vector and assemble into fl
        be = [(1 - (3*(2*x1 + x2))/(12*L)) (1 - (3*(x1 + 2*x2))/(12*L))];
        fe = ((rho*g*dx*Ao)/2)*be';
        fl(Edof(i, 2:n), 1) = fl(Edof(i, 2:n), 1) + fe;

        x1 = x1 + dx; x2 = x2 + dx;
    end

    %% define the boundary conditions

    % Essential (Dirichlet) boundary conditions
    bc = [1 0];             % displacement at x = 0 (u(0) = 0)
    
    % specify natural (Neumann) boundary condition in input to function

    %% calculate boundary load vector
    fb = zeros(ndof, 1);
    fb(end, 1) = F;         % define boundary load for Neumann BC

    %% establish system of equations and solve
    % sum boundary and element load vectors
    f = fb + fl;

    % solve system of equations
    [u, Q] = solveq(K, f, bc);
end