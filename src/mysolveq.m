function [a, Q] = mysolveq(K, f, bc, pc)
%mysolveq

% define the number of dofs
ndof = length(K);

% define the constrained dofs
cd = [bc; pc];

% define free dofs based on which dofs are constrained (i.e. take the
% remaining dofs from total list of dofs
fd = zeros(ndof - size(cd, 1), 1);
k = 1;
for i=1:ndof
    if ~ismember(i, cd(:,1))
        fd(k, 1) = i;
        k = k + 1;
    end
end

% partition the stiffness matrix
Kcc = K(cd(:,1), cd(:,1)); % corresponding to constrained dofs
Kff = K(fd(:, 1), fd(:, 1)); % correspondint to free dofs
Kfc = K(fd(:, 1), cd(:, 1));
Kcf = Kfc';

% define the known displacements
g = cd(:, 2);

% define the known forces
f = f(fd(:, 1));

% solve the system of equations as per the partioned matrix
d = Kff\(f-Kfc*g);
r = Kcc*g + Kcf*d;

a = zeros(ndof, 1);
a(fd(:,1)) = d;
a(cd(:,1)) = cd(:,2);
Q = zeros(ndof, 1);
Q(cd(:,1)) = r;

end