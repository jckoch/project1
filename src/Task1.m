% Project 1: Task 1
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%   
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

%% ---- TASK 1 ------------------------------------------------------------
% Truss finite element analysis
clear all; clc;

%% Generate the model
ndof = 24; % number of degrees of freedom
nele = 19; % number of elements

% define the topology matrix Edof
Edof = [1 1 2 3 4;
        2 3 4 5 6;
        3 5 6 7 8;
        4 7 8 9 10;
        5 9 10 11 12;
        6 11 12 13 14;
        7 3 4 15 16;
        8 3 4 17 18;
        9 5 6 17 18;
        10 7 8 17 18;
        11 7 8 19 20;
        12 7 8 21 22;
        13 9 10 21 22;
        14 11 12 21 22;
        15 11 12 23 24;
        16 15 16 17 18;
        17 17 18 19 20;
        18 19 20 21 22;
        19 21 22 23 24];

% define the element coordinates
ex = [0 3;
      3 9.5;
      9.5 16;
      16 22.5;
      22.5 29;
      29 32;
      3 3;
      3 9.5;
      9.5 9.5;
      16 9.5;
      16 16;
      16 22.5;
      22.5 22.5;
      29 22.5;
      29 29;
      3 9.5;
      9.5 16;
      16 22.5;
      22.5 29];
ey = [4 4;
      4 4;
      4 4;
      4 4;
      4 4;
      4 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 4;
      0 0;
      0 0;
      0 0;
      0 0];

% initialize the stiffness matrix K and load vector f
K = zeros(ndof);
fl = zeros(ndof,1);

% define the element properties
R = [0.025 0.010];  % m
A= pi*R.^2; % m2
E=200e9;  % GPa
ep=[E A];

% plot undeformed truss design
figure(1);
plotpar = [1, 2, 1];
elnum = Edof(:,1);
eldraw2(ex,ey,plotpar,elnum);
hold on;
 
%% ---- Stiffness Matrix Generation ---------------------------------------
% first step is to calculate the element stiffness matrices, Ke, and 
% assemble into K

red_A_elements = [9 11 13];
if isempty(red_A_elements)
    fprintf('Radius is %.3f mm^2\n', R(1)*10^3);
else
    fprintf('Elements with reduced x-section are ');
    for j = 1:length(red_A_elements)
        if j == length(red_A_elements)
            fprintf('%d\n', red_A_elements(j));
        else
            fprintf('%d, ', red_A_elements(j));
        end
    end
    fprintf('Radii are %.3f mm and %.3f mm\n', R(1)*10^3, R(2)*10^3);
    fprintf('\t,where the smaller radius correspond to\n\t the reduced x-section members\n\n');
end

for i=1:length(Edof)
    if ~ismember(i, red_A_elements)
        epi = ep(1:2);
    else
        epi = [ep(1) ep(3)];
    end
    Ke = bar2e(ex(i,:),ey(i,:),epi);
    K = assem(Edof(i,:),K,Ke);
end

% solve the system of equations with the given boundary conditions
bc = [1 0;2 0;13 0;14 0;15 0;16 0;23 0;24 0]; % BCs

%% ---- Task 1: A to D ----------------------------------------------------
% define applied load on truss
m = 1500; % kg
g = 9.81; % m^2/s
fl(8) = -m*g;

%------calclulate the total mass-------------------------------------------
L=zeros(1, length(A));
for i=1:length(Edof)
    if ~ismember(i, red_A_elements)
        L(1)=L(1)+sqrt((ex(i,2)-ex(i,1))^2+(ey(i,2)-ey(i,1))^2);
    else
        L(2)=L(2)+sqrt((ex(i,2)-ex(i,1))^2+(ey(i,2)-ey(i,1))^2);
    end
end
density = 7850; % steel S275, kg/m3
mass = (A(1)*L(1) + A(2)*L(2))*density;
fprintf("Total mass: %.2f kg\n", mass)

% solve the system of equations using CALFEM solveq function
[a, ~] = solveq(K, fl, bc);

% determine the element forces from the nodal reaction forces, Q
ed = extract(Edof,a);
N = zeros(length(Edof),1);
for i=1:length(Edof)
    N(i,:) = bar2s(ex(i,:),ey(i,:),ep,ed(i,:));
    % fprintf("%2.1f | %1.6f kN\n", i, N(i,:)/1000)
end

fprintf("Deflection at dof %2.1f is %1.6f mm\n", 8, a(8,:)*1000);
fprintf("\n");

%----- Draw deformed shape ------------------------------------------------
plotpar=[2,4,1];
eldisp2(ex,ey,ed,plotpar);
hold off;

%% ---- Task 1: E ---------------------------------------------------------
% define presribed displacement acting at midspan of the truss
pc = [8 -0.002]; % acting at the 8th dof

% solve the system of equations using MY OWN FUNCTION
[a, Q] = mysolveq(K, fl, bc, pc);

%----- Element forces -----------------------------------------------------
% determine the displacements per element
ed = extract(Edof,a);

%----- Draw deformed shape ------------------------------------------------
figure(2)
plotpar=[1,2,1];
eldraw2(ex,ey,plotpar,elnum);
hold on;

plotpar=[2,4,1];
eldisp2(ex,ey,ed,plotpar);
hold off;